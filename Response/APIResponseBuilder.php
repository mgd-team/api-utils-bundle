<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 05/03/2019
 * Time: 15:03
 */

namespace Flyshot\ApiUtilsBundle\Response;

use Flyshot\ApiUtilsBundle\Utils\RequestUtils;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class APIResponseBuilder
{
    private $serializer;

    private $response;

    private $requestStack;

    private $requestUtils;

    public function __construct(
        SerializerInterface $serializer,
        RequestStack $requestStack,
        RequestUtils $requestUtils
    )
    {
        $this->serializer = $serializer;
        $this->requestStack = $requestStack;
        $this->requestUtils = $requestUtils;

        $this->success();
    }

    public function success($data = null, $groups = []): self
    {
        $this->response = new APIResponse($this->serializer, $data, null, $groups, $this->getApiVersion());

        return $this;
    }

    public function error($error, $groups = []): self
    {
        $this->response = new APIResponse($this->serializer, null, $error, $groups, $this->getApiVersion());

        return $this;
    }

    public function getResponse(): APIResponse
    {
        return $this->response;
    }

    private function getApiVersion() : string
    {
        $request = $this->requestStack->getMasterRequest();
        if (null === $request) {
            return '';
        }

        return $this->requestUtils->getVersion($request);
    }
}
