API Utils Bundle
===================================

This is the Symfony bundle with utils, services and classes to format request and response data to JSON representation.

Main features: 

* UUID serialization handler
  
* API version check (based on a `accept` header)
  
* Form errors processing

Installation
-----------------------------------

    composer req stacklevel/api-utils-bundle

Basic usage
-----------------------------------

    namespace App\Controller;

    use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
    use Flyshot\ApiUtilsBundle\Response\APIResponseBuilder;
    
    class APIController extends AbstractController
    {
        public function test(APIResponseBuilder $builder)
        {
            return $this->builder->success("OK")->getResponse();
        }
    }
