<?php


namespace Flyshot\ApiUtilsBundle\Utils;

use Symfony\Component\HttpFoundation\Request;

class RequestUtils
{
    public function getVersion(Request $request): string
    {
        $header = $request->headers->get('Accept');
        preg_match('/v([0-9.]+)/', $header, $matches);

        return isset($matches[1]) ? $matches[1] : '';
    }

    public static function cmpVersions(string $v1, string $v2): int
    {
        $v1 = explode('.', $v1);
        $v2 = explode('.', $v2);

        for ($i = 0; $i < max(count($v1), count($v2)); $i++) {
            $p1 = $v1[$i] ?? 0;
            $p2 = $v2[$i] ?? 0;

            if ($p1 != $p2) {
                return $p1 > $p2 ? 1 : -1;
            }
        }

        return 0;
    }
}
