<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 2/24/20
 * Time: 4:11 PM
 */

namespace Flyshot\ApiUtilsBundle\Utils;

use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\JsonSerializationVisitor;
use JMS\Serializer\JsonDeserializationVisitor;
use JMS\Serializer\Context;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class UuidSerializerHandler implements SubscribingHandlerInterface
{
    public static function getSubscribingMethods()
    {
        return [
            [
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format' => 'json',
                'type' => 'Ramsey\Uuid\Uuid',
                'method' => 'serializeUuidToJson',
            ],
        ];
    }

    public function serializeUuidToJson(JsonSerializationVisitor $visitor, UuidInterface $data, array $type, Context $context)
    {
        return $data->toString();
    }
}